HyperdriveDesigns Protector
===================
Protector a simple Roles and Permissions system to Laravel's built in Auth system.

Documentation: Installation
-------------
to install: create a packages folder in main app directory. then unzip and place the new library package inside the packages folder:

Add the service provider and facade to your project's `config/app.php` file.

### Service Provider
```php
HyperdriveDesigns\Protector\ProtectorServiceProvider::class,
```

### Facades
```php
'Protector' => HyperdriveDesigns\Protector\Facades\Protector::class,
```
### Composer json file
Add this to your projects composer.json file:
```
"psr-4": {
    "App\\": "app/",
    "HyperdriveDesigns\\Protector\\": "packages/hyperdrivedesigns/protector/src"
}
```
then run 'composer dump-autoload'

### Middleware
```php
'rolesprotector' => \HyperdriveDesigns\Protector\Middleware\UserHasRole::class,
'permissionprotector' => \HyperdriveDesigns\Protector\Middleware\UserHasPermission::class,
```
### Migrations
now run below in Terminal to tell Laravel about the new package and create the new db tables:

php artisan vendor:publish --provider="HyperdriveDesigns\Protector\ProtectorServiceProvider"
php artisan migrate

I assume you have User model already created.
add this to the top of your User Model:

```php
use HyperdriveDesigns\Protector\Traits\ProtectorTrait;
```

and add the Trait to the User Model as such:

```php
class User extends Authenticatable
{
    use ProtectorTrait;
```

### Usage

for routes you would use it like this:
```php
Route::group( ['middleware' => 'permissionprotector:edit_widget' ], function() {
        Route::get('/home', 'HomeController@index');
});
```

for controllers:
```php
if(Auth::user()->isRole('administrator')){

}
```

for templates
```php
@role('admin')

@endrole

@can('edit_widget')

@endcan
```